#!/bin/bash

# Forked from:
# PCI_Configs - Checks Linux systems for PCI Compliance
# Copyright (C) 2014 Joseph Barcia - joseph@barcia.me
# https://github.com/jbarcia

# Needed Variables - DO NOT CHANGE
# ******************************************************************************
version=1.0
fdate=`date +%m.%d.%y-%H.%M`
# echo $fdate
USERPROFILE=$(eval echo ~${SUDO_USER})
# ******************************************************************************

clear
echo          RHEL8/CentOS evidence collector script v$version for:
echo          - PCI DSS v3.1 
echo          - NIST-800
echo --------------------------------------------------

echo Enter Host Name
read SiteName
echo Enter Auditor Name
read AuditorName

#Create a temp directory
#If having issues creating the directory, hard code where you would like the files stored
#rootdir=$USERPROFILE/Desktop
rootdir=./result
tempdir=PCIDSSv3.1-$fdate-$SiteName-$AuditorName
if [ ! -d "$rootdir" ]; then
	mkdir "$rootdir"
fi

cd $rootdir

if [ -d "$tempdir" ]; then
	echo *****WARNING: $rootdir/$tempdir already exists rename the folder to prevent data loss*****
	exit 1
fi

mkdir "$tempdir"

echo Collecting host profile to uname.txt
uname -a > $tempdir/uname.txt

echo Collecting Linux distro detail to distro.txt
cat /etc/*-release > $tempdir/host-profile.txt

echo Collecting files with incorrect hash to incorrect-hash.txt
sudo rpm -Va --nodeps --nosize --nomtime --nordev --nocaps --nolinkto --nouser --nogroup --nomode --noghost --noconfig | awk '{ printf("%s\n", $2); }' | sort | uniq > $tempdir/incorrect-hash.txt

echo Collecting list of package detail to packages.txt
#cat incorect-hash.txt | awk '{ printf("%s\n", $2); }' | sort | uniq
while read pkg; do
  echo "... Checking $pkg"
  rpm -qf $pkg$1 >> $tempdir/packages.txt
done < $tempdir/incorrect-hash.txt

echo Collecting list of files with incorrect permissions to incorrect-perm.txt
sudo rpm -Va --nodeps --nosignature --nofiledigest --nosize --nomtime --nordev --nocaps --nolinkto --nouser --nogroup > $tempdir/incorrect-perm.txt
#echo Create list of packages
#cat incorrect-perm.txt | awk '{ printf("%s\n", $3); }' | sort | uniq

echo Check if aide is installed to is-aide-installed.txt
rpm -qa | grep aide > $tempdir/is-aide-installed.txt

echo Check AIDE Database
sudo /usr/sbin/aide -C > $tempdir/aide-check.txt
#ls /var/lib/aide/aide.db.new.gz

echo Get crypto policy
/usr/bin/update-crypto-policies --show > $tempdir/crypto-policy.txt

echo Get /etc/ipsec.conf
sudo cat /etc/ipsec.conf > $tempdir/ipsec-conf.txt
#cat /etc/ipsec.conf | grep crypto-policies

echo Check crypto_policy group
#grep '^\s*\[\s*crypto_policy\s*]' /etc/pki/tls/openssl.cnf
sudo cat /etc/pki/tls/openssl.cnf > $tempdir/openssl-conf.txt

echo --------------------------------------------------
echo  Packaging up the Files
echo --------------------------------------------------
chown -R $USER:$USER $tempdir
tar cvzf "$tempdir.tar.gz" "$tempdir/"
rm -rf "$tempdir/"

echo .
echo .
echo Finish...
echo Evidence file is located here:
echo "$rootdir/$tempdir.tar.gz"
read -p "Press [Enter] key to continue..."

exit
