#!/bin/bash

# Forked from:
# PCI_Configs - Checks Linux systems for PCI Compliance
# Copyright (C) 2014 Joseph Barcia - joseph@barcia.me
# https://github.com/jbarcia

# Needed Variables - DO NOT CHANGE
# ******************************************************************************
version=1.0
fdate=`date +%m.%d.%y-%H.%M`
# echo $fdate
USERPROFILE=$(eval echo ~${SUDO_USER})
# ******************************************************************************

clear
echo          Evidence collector script v$version for DR/DRC section of:
echo          - POJK No.38/POJK.03/2016 
echo          - SEOJK No.21/SEOJK.03/2017
echo --------------------------------------------------

echo Enter Host Name
read SiteName
echo Enter Auditor Name
read AuditorName

#Create a temp directory
#If having issues creating the directory, hard code where you would like the files stored
#rootdir=$USERPROFILE/Desktop
rootdir=./result
tempdir=PBI-OJK-$fdate-$SiteName-$AuditorName
if [ ! -d "$rootdir" ]; then
	mkdir "$rootdir"
fi

cd $rootdir

if [ -d "$tempdir" ]; then
	echo *****WARNING: $rootdir/$tempdir already exists rename the folder to prevent data loss*****
	exit 1
fi

mkdir "$tempdir"

echo Collecting host profile to uname.txt
uname -a > $tempdir/uname.txt

echo Collecting Linux distro detail to distro.txt
cat /etc/*-release > $tempdir/host-profile.txt

echo Collecting /etc/passwd evidence to users.txt
sudo cat /etc/passwd > $tempdir/users.txt

echo Collecting /etc/login.defs evidence to setting.txt
sudo cat /etc/login.defs > $tempdir/setting.txt

echo Collecting network adapters evidence to ifconfig.txt
ifconfig -a > $tempdir/ifconfig.txt

echo Collecting /etc/shadow evidence to shadow.txt
sudo cat /etc/shadow > $tempdir/shadow.txt

echo Collecting /etc/group evidence to group.txt
sudo cat /etc/group > $tempdir/group.txt

echo Collecting / list files/dirs evidence to root_directory.txt
sudo ls -l / > $tempdir/root_directory.txt

echo --------------------------------------------------
echo  Packaging up the Files
echo --------------------------------------------------
chown -R $USER:$USER $tempdir
tar cvzf "$tempdir.tar.gz" "$tempdir/"
rm -rf "$tempdir/"

echo .
echo .
echo Finish...
echo Evidence file is located here:
echo "$rootdir/$tempdir.tar.gz"
read -p "Press [Enter] key to continue..."

exit
